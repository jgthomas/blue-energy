## Blue Energy meter reading API

Implementation of an API to submit and view meter readings.

### Submitting meter readings

Meter readings should be submitted in the following JSON format:

```

{
    "customerId": "identifier123",
    "serialNumber": "27263927192",
    "mpxn": "14582749",
    "read": [
        {"type": "ANYTIME", "registerId": "387373", "value": "2729"},
        {"type": "NIGHT", "registerId": "387373", "value": "2892"}
    ],
    "readDate": "2017-11-20T16:19:48+00:00Z"
}
```

All fields should be present, being named and formatted in the manner shown above.

##### Notes

* ***customerId*** and ***serialNumber*** should be alpha-numeric

* ***mpxn*** and ***registerId*** should be entirely numeric

* Only **ANYTIME** and **NIGHT** are accepted as read types

* The read ***value*** should be a positive integer, or zero

* ***readDate*** should be a valid ISO 8601 date format, and present the time of read to at least the granularity of seconds

### Querying meter readings

Queries to the API are made using three URL parameters:

* ***customerId*** -- the customer's unique identifier

* ***serialNumber*** -- the customer's meter serial number

* ***num*** -- the number of meter readings to retrieve

The first two parameters are mandatory.

The third parameter is optional. If ***num*** is not provided, then the system responds with the **most recent** meter reading for the supplied ***customerId*** and ***serialNumber***.

#### Making requests

To request the latest meter reading, send:

```
/route?customerId=10001&serialNumber=12345678
```

To request the last ten meter readings:

```
/route?customerId=10001&serialNumber=12345678&num=10
```

#### Response format

Responses are provided in a similar JSON format to submitted data. The overall structure of a response is shown below.

The response object contains a count of the number of meter readings returned, and a list containing the objects representing each of those readings. These objects contain the read data, and the system-generated id number of the meter read submission.

```
{
  "read-count": 1, 
  "readings": [
    {
      "read-data": {}, 
      "read-id": x
    },
    {
        
    },
    ...
  ]
}
```

#### Sample response for a single meter reading

```
{
  "read-count": 1, 
  "readings": [
    {
      "read-data": {
        "customerId": "10001", 
        "mpxn": "8768768", 
        "read": [
          {
            "registerId": "333", 
            "type": "ANYTIME", 
            "value": 8519
          }, 
          {
            "registerId": "333", 
            "type": "NIGHT", 
            "value": 4355
          }
        ], 
        "readDate": "2019-11-20T16:19:48+00:00Z", 
        "serialNumber": "12345678"
      }, 
      "read-id": 2
    }
  ]
}
```

#### Sample response for a request for the last two meter readings

```
{
  "read-count": 2, 
  "readings": [
    {
      "read-data": {
        "customerId": "10001", 
        "mpxn": "8768768", 
        "read": [
          {
            "registerId": "333", 
            "type": "ANYTIME", 
            "value": 8519
          }, 
          {
            "registerId": "333", 
            "type": "NIGHT", 
            "value": 4355
          }
        ], 
        "readDate": "2019-11-20T16:19:48+00:00Z", 
        "serialNumber": "12345678"
      }, 
      "read-id": 2
    }, 
    {
      "read-data": {
        "customerId": "10001", 
        "mpxn": "8768768", 
        "read": [
          {
            "registerId": "333", 
            "type": "ANYTIME", 
            "value": 5676
          }, 
          {
            "registerId": "333", 
            "type": "NIGHT", 
            "value": 1334
          }
        ], 
        "readDate": "2017-11-20T16:19:48+00:00Z", 
        "serialNumber": "12345678"
      }, 
      "read-id": 1
    }
  ]
}

```

## Limitations and possible improvements

* The API currently assumes that each customer is associated with only one meter serial number, and one MPXN number. In reality, this assumption is unlikely to be true.

* The system currently stores meter readings using SQLite. If it were to be used at scale, it would be necessary to move to a more robust database, such as PostgreSQL.

* Database access is currently managed using the Python package [dataset](https://dataset.readthedocs.io/en/latest/index.html), which provides an abstraction layer around SQLAlchemy. This makes database use very convenient, but I have already had to write some custom SQL for more complex queries, and so switching to a full ORM may be required.

* There is currently no security or authentication implemented on the API.
