
import dataset
from pathlib import Path

import data.fields as f


class DB:
    """
    Create a database object, and provide methods to update and
    query the database using the 'dataset' package, a abstraction
    layer over sqlalchemy.

    """
    dialect = 'sqlite:///'
    path = str(Path(__file__).parent.parent)
    customer_table_name = 'customer'
    reading_table_name = 'reading'

    def __init__(self, db_name=None):
        """
        Construct database instance.

        Args:
            db_name (string): name of database to create

        If no name is provided, an sqlite in-memory database is created,
        which can be used for testing and other puropses. If a name
        is supplied, an sqlite database file is created in the project's
        top-level directory.

        Creates two tables:
            customer : storing data on customers
            reading  : storing individual meter readings

        """
        if db_name is None:
            self.db_url = 'sqlite:///:memory:'
        else:
            self.db_name = db_name
            self.db_url = DB.dialect + DB.path + '/' + db_name
        self.db = dataset.connect(self.db_url)
        self.customer_table = self.db.create_table(
                DB.customer_table_name,
                primary_id=f.customer_id,
                primary_type=self.db.types.string)
        self.reading_table = self.db.create_table(
                DB.reading_table_name)

    def insert_customer(self, data):
        """ Insert record into customer table. """
        self._insert(data, self.customer_table)

    def insert_read(self, data):
        """ Insert record into meter reads table. """
        self._insert(data, self.reading_table)

    def _insert(self, data, table):
        """ Insert record into a table. """
        table.insert(data)

    def check_customer(self, cid, sn):
        """ Check if customer details stored. """
        customer_details = {
                f.customer_id : cid,
                f.serial_number : sn
                }
        return self._check_record(customer_details, self.customer_table)

    def check_read(self, cid, read_date):
        """ Check if meter read stored. """
        read_details = {
                f.customer_id : cid,
                f.read_date : read_date
                }
        return self._check_record(read_details, self.reading_table)

    def _check_record(self, record_details, table):
        """ Check existence of a record in a table. """
        return table.find_one(**record_details)

    def latest_reading(self, cid):
        """
        Return most recent meter reading data for customer.

        Args:
            cid (string): customer id

        Returns:
            Data object with details of the most recent meter reading,
            returned as only element in single-element list, for
            compatibility with results of last_readings().

        """
        query_string = most_recent_reading(DB.reading_table_name)
        query_params = {f.customer_id : cid}
        return self._custom_query(query_string, query_params)

    def last_readings(self, cid, num):
        """
        Return the num most recent meter readings for customer.

        Args:
            cid (string): customer id
            num (int): number of of most recent readings to return

        Returns:
            List of data Dobject with details of most recent meter reading.

        """
        query_string = last_n_readings(DB.reading_table_name)
        query_params = {f.customer_id: cid, f.num: num}
        return self._custom_query(query_string, query_params)

    def _custom_query(self, query_string, query_params):
        """ Run custom query with parameterised input. """
        result = [
                row for row in
                self.db.query(query_string, **query_params)
                ]
        return result


def most_recent_reading(table_name):
    """
    Return query string to find details of the most recent meter reading.

    Args:
        table_name (string): name of table to query

    """
    return f"""SELECT *
                 FROM {table_name}
                WHERE {f.read_date}=(
                          SELECT MAX({f.read_date})
                                FROM {table_name}
                               WHERE {f.customer_id} = :{f.customer_id})
                  AND {f.customer_id} = :{f.customer_id}"""


def last_n_readings(table_name):
    """
    Return query string to find details of the last n meter readings.

    Args:
        table_name (string): name of table to query

    """
    return f"""SELECT *
                 FROM {table_name}
                WHERE {f.customer_id} = :{f.customer_id}
             ORDER BY {f.read_date} DESC
                LIMIT :{f.num}"""
