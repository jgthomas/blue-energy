
from flask import (Flask,
                   request,
                   jsonify,
                   abort)

from api.db import DB
from api.validate import validate_meter_submission
from api.processing import process_submitted, build_response
import data.fields as f


def create_app(test=False):
    """
    Create instance of the flask app.

    Args:
        test (bool): database created in-memory if True, else in file.

    Returns:
        Instance of flask app.

    """
    app = Flask(__name__)

    if test:
        db = DB()
    else:
        db = DB('meter_reading.db')


    @app.route('/')
    def index():
        abort(403, "access forbidden")


    @app.route('/meter-read', methods=['GET', 'POST'])
    def meter_read():
        if request.method == 'POST':
            submitted = request.get_json()
            if validate_meter_submission(submitted):
                customer_data, read_data = process_submitted(submitted)
                cid = customer_data[f.customer_id]
                msn = customer_data[f.serial_number]
                date = read_data[f.read_date]

                # Only store customer if not already in customer table
                if not db.check_customer(cid, msn):
                    db.insert_customer(customer_data)

                # Only store reading data once for a given timestamp
                if not db.check_read(cid, date):
                    db.insert_read(read_data)
                else:
                    abort(400, "read data for supplied timestamp already submitted")

                return jsonify({'status': 'success'})
            abort(400, "bad data!")
        abort(403, "access forbidden")


    @app.route('/present-read', methods=['GET'])
    def customer():
        cid = request.args.get(f.customer_id, default=None, type=str)
        sn = request.args.get(f.serial_number, default=None, type=str)
        num = request.args.get(f.num, default=1, type=int)
        if cid and sn:
            cust = db.check_customer(cid, sn)
            if cust:
                if num > 1:
                    reads = db.last_readings(cid, num)
                else:
                    reads = db.latest_reading(cid)
                response = build_response(cust, reads)
                return jsonify(response)
            abort(400, f'invalid "{f.customer_id}" and/or "{f.serial_number}"')
        abort(400, f'missing "{f.customer_id}" and/or "{f.serial_number}"')

    return app


if __name__ == "__main__":
    app = create_app(test=False)
    app.run()
