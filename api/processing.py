
"""
Module contains functions for processing the incoming JSON data, and for
formatting the JSON responses in requests for meter read data.

"""

import data.fields as f


def process_submitted(submitted):
    """
    Process the supplied json data, converting it into two dictionary
    objects for easy insertion into the database.

    Args:
        submitted (dict): data parsed from the submitted JSON file.

    Returns:
        Two dictionaries, one containing data on the customer, the other
        on the meter readings themselves.

    """
    customer_fields = [
            f.customer_id,
            f.serial_number,
            f.mpxn]

    read_fields = [
            f.read_date,
            f.customer_id,
            f.register_id,
            f.anytime,
            f.night]

    anytime, night = submitted[f.read]
    submitted = {i:submitted[i] for i in submitted if i != f.read}

    anytime_read = {
            anytime[f.read_type]: int(anytime[f.read_value]),
            f.register_id: anytime[f.register_id]
            }
    night_read = {
            night[f.read_type]: int(night[f.read_value])
            }

    data = {**submitted, **anytime_read, **night_read}
    customer_data = {i:data[i] for i in data if i in customer_fields}
    read_data = {i:data[i] for i in data if i in read_fields}

    return (customer_data, read_data)


def build_response(customer_data, read_data):
    """ Return object with data on all requested meter reading submissions. """
    response = []
    for read in read_data:
        summary = {
                f.read_id: read[f.read_db_id],
                f.read_data: _build_single_response(customer_data, read)
                }
        response.append(summary)
    return {
            f.readings: response,
            f.read_count: len(response)
            }


def _build_single_response(customer_data, read_data):
    """ Return object with data on single meter reading submission. """
    response = {
            f.customer_id: customer_data[f.customer_id],
            f.serial_number: customer_data[f.serial_number],
            f.mpxn: customer_data[f.mpxn],
            f.read: _build_read(read_data),
            f.read_date: read_data[f.read_date]
            }
    return response


def _build_read(read_data):
    """ Return object with data on single pair of ANYTIME and NIGHT readings. """
    anytime = {
            f.read_type: f.anytime,
            f.register_id: read_data[f.register_id],
            f.read_value: read_data[f.anytime]
            }
    night = {
            f.read_type: f.night,
            f.register_id: read_data[f.register_id],
            f.read_value: read_data[f.night]
            }
    return [anytime, night]
