
"""
Module contains functions for validating the submitted JSON data.

Validation is conducted against a schema, to check that all data is
present and of the correct type, and against the content of the data
to ensure it has the required characteristics.

"""

import jsonschema as jsch
import datetime as dt
import dateutil.parser

import data.fields as f


schema = {
        "type": "object",
        "properties": {
            f.customer_id: {"type" : "string"},
            f.serial_number: {"type" : "string"},
            f.mpxn: {"type" : "string"},
            f.read_date: {"type" : "string"},
            }, "required": [
                f.customer_id,
                f.serial_number,
                f.mpxn,
                f.read_date
                ]
        }


read_schema = {
        "type": "object",
        "properties": {
            f.read_type: {"type" : "string"},
            f.register_id: {"type" : "string"},
            f.read_value: {"type" : "string"},
            }, "required": [
                f.read_type,
                f.register_id,
                f.read_value
                ]
        }


def validate_meter_submission(submitted):
    """ Check submitted data is in the correct format. """
    read = submitted.get(f.read, None)
    customer_check = _validate_against_schema(submitted, schema)
    read_check = _validate_read(read) if read else False
    data_check = False
    if customer_check and read_check:
        data_check = _validate_data(submitted, read)
    return customer_check and read_check and data_check


def _validate_read(read):
    """ Check meter readings correct. """
    return (_validate_read_length(read)
            and _validate_read_format(read)
            and _validate_read_order(read))


def _validate_read_length(read):
    """ Check both meter readings present. """
    return len(read) == 2


def _validate_read_order(read):
    """ Check meter readings ordered correctly. """
    return (read[0][f.read_type] == f.anytime
            and read[1][f.read_type] == f.night)


def _validate_read_format(read):
    """ Check meter readings formatted correctly. """
    return all(_validate_against_schema(r, read_schema) for r in read)


def _validate_against_schema(data, schema):
    """ Validate json object against a schema. """
    try :
        jsch.validate(data, schema=schema)
        return True
    except jsch.ValidationError:
        return False


def _validate_data(submitted, read):
    """ Check validity of actual data submitted, as opposed to just the schema. """
    cust_data_check = _validate_customer_data(submitted)
    read_data_check = _validate_read_data(read)
    return cust_data_check and read_data_check


def _validate_customer_data(submitted):
    """ Check validity of data associated with a customer. """
    date_check = is_isodate(submitted[f.read_date])
    mpxn_check = _is_mpxn(submitted[f.mpxn])
    cid_check = _is_customer_id(submitted[f.customer_id])
    msn_check = _is_meter_sn(submitted[f.serial_number])
    return date_check and mpxn_check and cid_check and msn_check


def _validate_read_data(read):
    """ Check validity of data associated with a reading. """
    read_value_check = all(is_valid_meter_read(r[f.read_value]) for r in read)
    read_type_check = all(_is_read_type(r[f.read_type]) for r in read)
    read_regid_check = all(_is_register_id(r[f.register_id]) for r in read)
    return read_value_check and read_type_check and read_regid_check


def _is_mpxn(mpxn):
    """
    Check meter point number is valid.

    MPAN - electricity, 13 digits long
    MPRN - gas, up to 11 digits

    """
    return mpxn.isnumeric() and len(mpxn) < 14


def _is_customer_id(cid):
    """ Check customer ID is valid. """
    return cid.isalnum()


def _is_meter_sn(sn):
    """ Check meter serial number is valid. """
    return sn.isalnum()


def _is_register_id(reg_id):
    """ Check register ID is valid. """
    return reg_id.isnumeric()


def _is_read_type(read_type):
    """ Check read types are valid. """
    valid_types = [f.anytime, f.night]
    return read_type in valid_types


def is_isodate(date_string):
    """
    Check if string can be interpreted as an ISO 8601 date

    note: dateutil does not support offsets of *both* 00:00 and Z
    when used together, so any trailing 'Z' is stripped before parsing

    """
    if date_string[-1] == 'Z':
        date_string = date_string[:-1]
    try:
        dateutil.parser.isoparse(date_string)
        return _date_well_formatted(date_string)
    except ValueError:
        return False


def _date_well_formatted(date_string):
    """ Check date format is at least to granularity of seconds. """
    # remove any ISO 8601 offset to check date format
    date_string = date_string.strip().split('+')[0]
    required_format = '%Y-%m-%dT%H:%M:%S'
    try:
        dt.datetime.strptime(date_string, required_format)
        return True
    except ValueError:
        return False


def is_valid_meter_read(value):
    """ Check if meter reading is a positive integer. """
    try:
        n = int(value)
        return n > -1
    except ValueError:
        return False
