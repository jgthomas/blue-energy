
"""
Unit tests of validation functions.

"""

import json
import pytest

import api.validate as valid
import data.dummy_data as dummy


def test_valid_full_submission():
    data = dummy.valid_submission
    assert valid.validate_meter_submission(data) == True


def test_invalid_read_submission():
    data = dummy.invalid_read
    assert valid.validate_meter_submission(data) == False


def test_missing_read_date_submission():
    data = dummy.missing_read_date
    assert valid.validate_meter_submission(data) == False


def test_missing_all_readings():
    data = dummy.no_readings
    assert valid.validate_meter_submission(data) == False


def test_valid_isodate_string():
    date_string = '2019-11-20T16:19:48+00:00Z'
    assert valid.is_isodate(date_string) == True


def test_invalid_isodate_string():
    date_string = '11-20T16:19:48+00:00Z'
    assert valid.is_isodate(date_string) == False


def test_date_missing_seconds():
    date_string = '2019-11-20T16:19'
    assert valid.is_isodate(date_string) == False


def test_valid_meter_readings():
    reads = [
            '1000',
            '2345',
            '+4566'
            ]
    for read in reads:
        assert valid.is_valid_meter_read(read) == True


def test_invalid_meter_readings():
    reads = [
            '-1000',
            '1234a',
            'asdfgg'
            ]
    for read in reads:
        assert valid.is_valid_meter_read(read) == False
