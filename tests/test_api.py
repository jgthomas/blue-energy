
"""
Functional tests of API endpoints and responses.

"""

import pytest

from api.meter_api import create_app
import data.dummy_data as dummy
import data.fields as f


accept_url = '/meter-read'
present_url = '/present-read'


app = create_app(test=True)


@pytest.fixture
def client():
    app.config['TESTING'] = True

    with app.test_client() as client:
        with app.app_context():
            yield client


def test_unathorised_get_to_index(client):
    r = client.get('/')
    assert r.status_code == 403


def test_unauthorised_get_to_submission_route(client):
    r = client.get('/meter-read')
    assert r.status_code == 403


def test_full_json_submission(client):
    r = client.post(accept_url, json=dummy.valid_submission)
    assert r.status_code == 200


def test_invalid_json_submission(client):
    r = client.post(accept_url, json=dummy.invalid_read)
    assert r.status_code == 400


def test_missing_read_date_submission(client):
    r = client.post(accept_url, json=dummy.missing_read_date)
    assert r.status_code == 400


def test_missing_no_readings_submission(client):
    r = client.post(accept_url, json=dummy.no_readings)
    assert r.status_code == 400


def test_check_customer_reading(client):
    r = client.get(
            f'/present-read?{f.customer_id}=10001&{f.serial_number}=12345678'
            )
    json_data = r.get_json()
    assert json_data[f.readings][0][f.read_data][f.mpxn] == '8768768'
    assert r.status_code == 200


def test_check_invalid_cid_reading(client):
    r = client.get(
            f'/present-read?{f.customer_id}=22222&{f.serial_number}=12345678'
            )
    assert r.status_code == 400


def test_check_invalid_sn_reading(client):
    r = client.get(f'/present-read?{f.customer_id}=10001&{f.serial_number}=666')
    assert r.status_code == 400


def test_check_missing_cid_reading(client):
    r = client.get(f'/present-read?{f.serial_number}=12345678')
    assert r.status_code == 400


def test_check_missing_sn_reading(client):
    r = client.get(f'/present-read?{f.customer_id}=10001')
    assert r.status_code == 400


def test_check_missing_all_info_reading(client):
    r = client.get('/present-read')
    assert r.status_code == 400


def test_latest_meter_reading_supplied(client):
    # post second, more recent, valid meter reading
    r = client.post(accept_url, json=dummy.second_valid_submission)

    # request latest meter reading
    r = client.get(
            f'/present-read?{f.customer_id}=10001&{f.serial_number}=12345678'
            )

    json_data = r.get_json()
    assert json_data[f.readings][0][f.read_data][f.read][0][f.read_value] == 8519
    assert json_data[f.readings][0][f.read_data][f.read][1][f.read_value] == 4355
    assert r.status_code == 200


def test_request_multiple_readings(client):
    r = client.get(
            f'/present-read?{f.customer_id}=10001&{f.serial_number}=12345678&{f.num}=2'
            )
    json_data = r.get_json()
    assert json_data[f.readings][0][f.read_data][f.read][0][f.read_value] == 8519
    assert json_data[f.readings][0][f.read_data][f.read][1][f.read_value] == 4355
    assert json_data[f.readings][1][f.read_data][f.read][0][f.read_value] == 5676
    assert json_data[f.readings][1][f.read_data][f.read][1][f.read_value] == 1334
    assert r.status_code == 200


def test_request_more_readings_than_available(client):
    r = client.get(
            f'/present-read?{f.customer_id}=10001&{f.serial_number}=12345678&{f.num}=10'
            )
    json_data = r.get_json()
    assert len(json_data[f.readings]) == 2
    assert r.status_code == 200


def test_invalid_date_format_submission(client):
    r = client.post(accept_url, json=dummy.invalid_date_format)
    assert r.status_code == 400


def test_invalid_meter_read_format_submission(client):
    r = client.post(accept_url, json=dummy.invalid_reading_format)
    assert r.status_code == 400


def test_cannot_insert_two_readings_with_same_timestamp(client):
    # insert same record with same time as an earlier test
    r = client.post(accept_url, json=dummy.valid_submission)
    assert r.status_code == 400
