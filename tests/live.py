
"""
Basic test of POST and GET to a real database file, requires
that the flask server is running.

"""

import json
import requests as req

import data.dummy_data as dummy
import data.fields as f


url = 'http://127.0.0.1:5000/'
post = 'meter-read'
get = 'present-read'

resp = req.post(
        url + post,
        json.dumps(dummy.valid_submission),
        headers={'Content-Type': 'application/json'}
        )
print(resp.text)


resp1 = req.post(
         url + post,
         json.dumps(dummy.second_valid_submission),
         headers={'Content-Type': 'application/json'}
         )
print(resp1.text)


resp2 = req.get(url + get + f'?{f.customer_id}=10001&{f.serial_number}=12345678')
print(resp2.text)
print(resp2.text, file=open("first_response.json", "a"))


resp3 = req.get(url + get + f'?{f.customer_id}=10001&{f.serial_number}=12345678&{f.num}=2')
print(resp3.text)
print(resp3.text, file=open("second_response.json", "a"))
