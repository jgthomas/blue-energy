
"""
Sample data that can be submitted to the API for testing and other purposes.

"""

import data.fields as f


valid_submission = {
        f.customer_id: "10001",
        f.serial_number: "12345678",
        f.mpxn: "8768768",
        f.read: [
            {f.read_type: "ANYTIME", f.register_id: "333", f.read_value: "5676"},
            {f.read_type: "NIGHT", f.register_id: "333", f.read_value: "1334"}
            ],
        f.read_date: "2017-11-20T16:19:48+00:00Z"
        }


second_valid_submission = {
        f.customer_id: "10001",
        f.serial_number: "12345678",
        f.mpxn: "8768768",
        f.read: [
            {f.read_type: "ANYTIME", f.register_id: "333", f.read_value: "8519"},
            {f.read_type: "NIGHT", f.register_id: "333", f.read_value: "4355"}
            ],
        f.read_date: "2019-11-20T16:19:48+00:00Z"
        }


invalid_read = {
        f.customer_id: "10001",
        f.serial_number: "12345678",
        f.mpxn: "8768768",
        f.read: [
            {f.register_id: "333", f.read_value: "5676"},
            {f.read_type: "NIGHT", f.register_id: "333", f.read_value: "1334"}
            ],
        f.read_date: "2017-11-20T16:19:48+00:00Z"
        }


missing_read_date = {
        f.customer_id: "10001",
        f.serial_number: "12345678",
        f.mpxn: "8768768",
        f.read: [
            {f.read_type: "ANYTIME", f.register_id: "333", f.read_value: "5676"},
            {f.read_type: "NIGHT", f.register_id: "333", f.read_value: "1334"}
            ]
        }


no_readings = {
        f.customer_id: "10001",
        f.serial_number: "12345678",
        f.mpxn: "8768768",
        f.read_date: "2017-11-20T16:19:48+00:00Z"
        }


invalid_date_format = {
        f.customer_id: "10001",
        f.serial_number: "12345678",
        f.mpxn: "8768768",
        f.read: [
            {f.read_type: "ANYTIME", f.register_id: "333", f.read_value: "5676"},
            {f.read_type: "NIGHT", f.register_id: "333", f.read_value: "1334"}
            ],
        f.read_date: "11-20T16:19:48+00:00Z"
        }


invalid_reading_format = {
        f.customer_id: "10001",
        f.serial_number: "12345678",
        f.mpxn: "8768768",
        f.read: [
            {f.read_type: "ANYTIME", f.register_id: "333", f.read_value: "X676"},
            {f.read_type: "NIGHT", f.register_id: "333", f.read_value: "1334"}
            ],
        f.read_date: "2017-11-20T16:19:48+00:00Z"
        }
