
"""
Module defines the fields of the submitted and return JSON files.

"""

# Fields in JSON submission
customer_id = 'customerId'
serial_number = 'serialNumber'
mpxn = 'mpxn'
read = 'read'
read_type = 'type'
read_value = 'value'
register_id = 'registerId'
read_date = 'readDate'

# Categories of meter readings
anytime = 'ANYTIME'
night = 'NIGHT'

# Meter read id for db
read_db_id = 'id'

# Additional options for GET query on data
num = 'num'

# Fields in JSON response, when asked for meter read
readings = 'readings'
read_count = 'read-count'
read_id = 'read-id'
read_data = 'read-data'
